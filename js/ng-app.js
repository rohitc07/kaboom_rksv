var app = angular.module('myApp', ['ngRoute']);

app.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'scrip.html',
            controller: 'scripController'
        })

});

app.controller('scripController', function($scope, $http, $q, $location, $timeout) {
    $scope.graphData = [];
    var chartUpdate;
    $scope.currentPrice = 0;
    $scope.dayHigh = 0;
    $scope.dayLow = 0;
    $scope.open = 0;
    $scope.close = 0;
    $scope.avgVolume = 0;
    $scope.totalVolume = 0;
    $scope.weekH52 = 0;
    $scope.weekL52 = 0;
    const socket = io.connect('http://kaboom.rksv.net/watch');
    var hd = fetchHistoryData().then(function(charts) {
        console.log("done");

        $timeout(function() {
            socket.emit("sub", {
                state: true
            });
        }, 5000);
        chartUpdate = charts;
    })
    socket.on('connect', function() {
        console.log("socket conected");
    });
    socket.on('data', function(data, ack) {
        console.log('Response: ' + data);
        var data = data.split(",")
        var obj = {};
        obj.date = new Date(parseInt(data[0]));
        obj.open = data[1];
        obj.high = data[2];
        obj.low = data[3];
        obj.close = data[4];
        obj.volume = data[5];
        $scope.currentPrice = data[1];
        if ($scope.weekL52 < parseInt(data[3])) {
            $scope.weekL52 = parseInt(data[3])
        }
        if ($scope.weekH52 < parseInt(data[2])) {
            $scope.weekH52 = parseInt(data[2])
        }
        if ($scope.dayLow < parseInt(data[3])) {
            $scope.dayLow = parseInt(data[3])
        }
        if ($scope.dayHigh < parseInt(data[2])) {
            $scope.dayHigh = parseInt(data[2])
        }
        $scope.totalVolume = $scope.totalVolume + parseInt(data[5]);
        console.log(obj);
        $scope.graphData.push(obj);
        $scope.avgVolume = $scope.totalVolume / ($scope.graphData.length - 399)
        $scope.close = data[4];
        if ($scope.open == 0) {
            $scope.open = data[1]
        }
        $scope.safeApply();
        reDraw(chartUpdate);
        console.log($scope.graphData.length);
        console.log($scope.graphData[0]);
        $timeout(function() {
            ack(1);
        }, 8000);

    });

    $timeout(function() {
        socket.emit('unsub', {
            state: false
        });
    }, 300000);


    socket.on('error', function(error) {
        console.error('Error: ' + error);
    });

    function drawCharts(Adata) {
        var chart = AmCharts.makeChart("chartContainer", {
            "type": "serial",
            "theme": "light",
            //"dataDateFormat":"YYYY-MM-DD",
            "valueAxes": [{
                "position": "left"
            }],
            "graphs": [{
                "id": "g1",
                "balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",
                "closeField": "close",
                "fillColors": "#7f8da9",
                "highField": "high",
                "lineColor": "#7f8da9",
                "lineAlpha": 1,
                "lowField": "low",
                "fillAlphas": 0.9,
                "negativeFillColors": "#db4c3c",
                "negativeLineColor": "#db4c3c",
                "openField": "open",
                "title": "Price:",
                "type": "candlestick",
                "valueField": "close"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "graphType": "line",
                "scrollbarHeight": 30
            },
            "chartCursor": {
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true
            },
            "dataProvider": Adata,

            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });

        chart.addListener("rendered", zoomChart);
        zoomChart();
        // this method is called when chart is first inited as we listen for "dataUpdated" event
        function zoomChart() {
            // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
            chart.zoomToIndexes(chart.dataProvider.length - 10, chart.dataProvider.length - 1);
        }

        return chart;
    }
    //to redraw the chart based on realtime data
    function reDraw(chart) {
        console.log("redraw called");
        chart.validateData();
        //changed period to seconds as data is received in seconds
        chart.categoryAxis.minPeriod = "ss";
        chart.zoomToIndexes(chart.dataProvider.length - ($scope.graphData.length - 399), chart.dataProvider.length - 1);
    }

    function fetchHistoryData() {
        var def = $q.defer();
        $http.get("http://kaboom.rksv.net/api/historical?interval=2").then(function(response) {

            for (let x = response.data.length - 1; x > 0; x--) {
                var data = response.data[x].split(",")
                var obj = {};
                obj.date = new Date(parseInt(data[0]));
                obj.open = data[1];
                obj.high = data[2];
                obj.low = data[3];
                obj.close = data[4];
                obj.volume = data[5];
                if ($scope.weekL52 < parseInt(data[3])) {
                    $scope.weekL52 = parseInt(data[3])
                }
                if ($scope.weekH52 < parseInt(data[2])) {
                    $scope.weekH52 = parseInt(data[2])
                }
                $scope.graphData.push(obj);

            }
            $scope.safeApply();
            //$scope.graphData = response.data;
            def.resolve(drawCharts($scope.graphData));
        });
        return def.promise;
    }
    $scope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    $scope.$on('$destroy', function() {
        console.log("unloaded");
        socket.emit('unsub', {
            state: false
        });
        socket.disconnect();
    })

}).filter('bigNumber', function() {
    return function(number) {
        if (isNaN(number) || number < 1) {
            return number;

        } else {
            abs = Math.abs(number);
            if (abs >= Math.pow(10, 12)) {
                // trillion
                number = (number / Math.pow(10, 12)).toFixed(1) + "T";
            } else if (abs < Math.pow(10, 12) && abs >= Math.pow(10, 9)) {
                // billion
                number = (number / Math.pow(10, 9)).toFixed(1) + "B";
            } else if (abs < Math.pow(10, 9) && abs >= Math.pow(10, 6)) {
                // million
                number = (number / Math.pow(10, 6)).toFixed(1) + "M";
            } else if (abs < Math.pow(10, 6) && abs >= Math.pow(10, 3)) {
                // thousand
                number = (number / Math.pow(10, 3)).toFixed(1) + "K";
            }
            return number;
        }
    }
});