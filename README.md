# kaboom_rksv
 
#Instructions:
install node.js
install http-server using the command npm install -g http-server.
browse to localhost:8080/ on your broswer.

# About the project.
there is a delay of 5 seconds for the websocket emit, to plot the history data first, which gets replaced with realtime data on socket connection.

there is a delay of 8 seconds between a socket receive and an ack.

the socket will automatically unsubscribe itself after 5 minutes, you'll have to reload the page to view it again.

Also since the project as created as a mobile view screen, its recommended to view the page using the developer tools mobile device toolbar
